<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'testdb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$NoTm:6^lS?5{0,s2dk,@y<?rLXzP&_J% 7+:!q8 1^=vn1cD9Jtq(yO[Wpna$D1');
define('SECURE_AUTH_KEY',  'Fyc>J9Tr=OF 3+<[3BqMJ-d?R@ 1!lb-oZx6*.ca >HZ9C-d-a07UN(}r*5yZ$d?');
define('LOGGED_IN_KEY',    'BnlD&u5$TFl.s5G+m/qY$CQP.(yHEL4ts9,0uNy,T}UGb(hHr26jjG!`HGOn!G7:');
define('NONCE_KEY',        'o7=Dc~kO%;+,le9S7Dmlr%akKVLE*_0Gy[J:?@.CcL-TApLb061B;NnG~?:]DYQ ');
define('AUTH_SALT',        'YSLDoEY#{}W3( DNTrTg.|o,:q*`N!c.V-,zO)qZ0JVf97Dg10(2b{>bv_7#EZNt');
define('SECURE_AUTH_SALT', '9QL64PcpC^]IR/*Yf;T6szp8%m8:WV_8u$jFDr@k=w8@|C<GGlosWl{Ro};>GT*O');
define('LOGGED_IN_SALT',   'X:@J#|?k)3w9s7SLOV_>S:=J!5B/}Tg;6!D^s,6D^9{0t_Q4~]}/.6 37fGI<yoX');
define('NONCE_SALT',       'q#t]lQ-%wbM3u)20;6$A|xGPiaW0I9Bs}b+pY<%wr]^%3luAo3j|O##+D528[w{]');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'tr_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
